const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
var app = express();



hbs.registerHelper('getCurrentYear',()=>{
  return new Date().getFullYear();
});

hbs.registerHelper('scremIt',(text)=>{
  console.log(text);
   return text.toUpperCase();
});


app.set('view engine','hbs');

console.log(__dirname);
app.use(express.static(__dirname + '/public'));

app.use((req,res,next)=>{
  var now = new Date().toString();
  var log =`${now}:${req.method} ${req.url}`;
  fs.appendFile('server.log',log +'\n',(err)=>{
    if(err)
    {
      console.log('unable to append server.log');
    }
  });
  console.log(log);
  next();
});

// app.use((req,res,next)=>{
//   res.render('maintenance.hbs');
// });

hbs.registerPartials(__dirname +'/views/partials');
 app.get('/',(req,res)=>{
   //res.send('Hello express!')
   res.render('home.hbs',{
     pageTitle:'Home page',
     WelcomMessage:'Welcome to my website'
   });
 });

app.get('/about',(req,res) =>{
   res.render('about.hbs',{
     pageTitle:'About page',
   });
});

app.get('/bad',(req,res)=>{
   res.send({
     status:'error',
     status_error_msg:'not supported'
   });
});

app.listen(3000,()=>{
  console.log('server is up on port 3000');
});
